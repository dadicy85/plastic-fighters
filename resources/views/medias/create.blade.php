<x-app-layout>
    <div class="tm-main">
        <div class="tm-section-wrap">
            <div class="tm-parallax" data-parallax="scroll" data-image-src="{{ asset('img/slide4.jpg') }}">
            </div>
            <section id="home" class="tm-section">
                <h2 class="tm-text-primary">Création d'un média</h2>
                <hr class="mb-5">
                <form method="POST" action="{{ route('medias.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="mt-4">
                        <x-jet-label for="title" value="{{ __('Titre') }}" />
                        <x-jet-input id="title" class="block mt-1 w-full" type="text" name="title" :value="old('title')"
                            placeholder="Titre" required />
                    </div>

                    <div class="mt-4">
                        <x-jet-label for="title2" value="{{ __('Titre2') }}" />
                        <x-jet-input id="title2" class="block mt-1 w-full" type="text" name="title2" :value="old('title2')"
                            placeholder="Titre2" required />
                    </div>

                    <div class="mt-4">
                        <x-jet-label for="link" value="{{ __('Lien') }}" />
                        <x-jet-input id="link" class="block mt-1 w-full" type="text" name="link" :value="old('link')"
                            placeholder="Lien" required />
                    </div>

                    <div class="mt-4">
                        <div class="custom-file">
                            <label for="validatedCustomFile" class="custom-file-label">Choisir une image</label>

                            <input type="file" class="custom-file-input @error('message') is-invalid @enderror"
                                name="image" id="validatedCustomFile">
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="flex items-center justify-end mt-4">
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="/index">
                            {{ __('Annuler') }}
                        </a>

                        <x-jet-button class="ml-4">
                            {{ __('Poster') }}
                        </x-jet-button>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</x-app-layout>
