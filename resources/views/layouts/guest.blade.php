<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#87CEFA" />
    <meta name="description" content="Collectif de rammassage des déchets sur les plages.">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" defer>
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('slick/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    @livewireStyles

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.singlePageNav.min.js') }}"></script>
    <script src="{{ asset('js/parallax/parallax.min.js') }}"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('slick/slick.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>

</head>

<body>
    <x-jet-banner />

    <div class="min-h-screen">
        {{-- @auth
        @livewire('navigation-menu')
        @endauth --}}

        <!-- Page Heading -->
        @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>
        @endif

        <!-- Page Content -->
        <main>
            <div class="container-fluid">
                <div class="row">
                    <!-- Menu Latéral -->
                    <div id="tm-sidebar" class="tm-sidebar">
                        <nav class="tm-nav">
                            <button class="navbar-toggler" type="button" aria-label="Toggle navigation">
                                <i class="fas fa-bars"></i>
                            </button>
                            <div>
                                <div class="tm-brand-box">
                                    <a href="/index"><img src="{{ asset('img/logo.jpg') }}" class="logohome"
                                            alt="Plastic Fighters" style="width: 30vh"></a>
                                </div>
                                <ul id="tm-main-nav">
                                    <li class="nav-item">
                                        @if(Auth::user())
                                        <a href="{{ route('dashboard') }}" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-user nav-icon"></i>
                                            Profil
                                        </a>
                                        @else
                                        <a href="{{ route('login') }}" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-user nav-icon"></i>
                                            Connexion
                                        </a>
                                        @endif
                                    </li>
                                    <li class="nav-item">
                                        <a href="/index#home" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-home nav-icon"></i>
                                            Accueil
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/index#media" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-microphone nav-icon"></i>
                                            Médias
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/index#gallery" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-images nav-icon"></i>
                                            Galeries
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/index#about" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-user-friends nav-icon"></i>
                                            À Propos
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/index#contact" class="nav-link external">
                                            <div class="triangle-right"></div>
                                            <i class="fas fa-envelope nav-icon"></i>
                                            Contact
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    {{ $slot }}
        </main>
    </div>
    </div>
    </div>
    @stack('modals')

    @livewireScripts

    <footer id="rezo">
        <!-- Copyright & Réseaux Sociaux-->
        <div class="text-center p-3" style="background-color: aliceblue;">
            <ul class="tm-contact-social">
                <li>
                    SUIVEZ NOUS SUR LES RESEAUX &nbsp;&nbsp;<i class='fas fa-angle-double-right'></i>
                </li>&nbsp;
                <li><a href="https://www.facebook.com/groups/822613485148449" class="tm-social-link"
                        onclick="window.open(this.href);return false"><i class="fab fa-facebook fa-2x"></i></a>
                </li>&nbsp;
                {{-- <li><a href="https://twitter.com" class="tm-social-link"
                        onclick="window.open(this.href);return false"><i class="fab fa-twitter fa-2x"></i></a>
                </li>&nbsp; --}}
                <li><a href="https://www.instagram.com/plasticfighters33/" class="tm-social-link"
                        onclick="window.open(this.href);return false"><i class="fab fa-instagram fa-2x"></i></a>
                </li>&nbsp;
                {{-- <li><a href="https://discord.com/channels/939925726096945242/939925938102231173" class="tm-social-link"
                        onclick="window.open(this.href);return false"><i class="fab fa-discord fa-2x"></i></a>
                </li>&nbsp; --}}
                <li><a href="https://www.youtube.com/channel/UCv9UDGlvqRxLPJ_XwsaScWQ" class="tm-social-link"
                        onclick="window.open(this.href);return false"><i class="fab fa-youtube fa-2x"></i></a>
                </li>
            </ul>
            <div class="text-right">
                <a href="">Conditions Générales d'Utilisation</a><br />
                © 2022 Copyright:
                <a class="text-blue" href="" onclick="window.open(this.href);return false">Dadicy</a> - 
                Design: <a href="https://templatemo.com" target="_parent" rel="sponsored"><i>TemplateMo</i></a>
            </div>
        </div>
        <!-- Fin Copyright & Réseaux Sociaux-->
    </footer>
</body>

</html>
