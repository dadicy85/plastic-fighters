<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <img src="{{ asset('img/logo.jpg') }}" style="width: 25vh" alt="Plastic Fighters">
        </x-slot>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="media">
                    <div class="media-body">
                        <img src="img/letter.png" class="poster align-self-start mr-3" style="width: 20vh" alt="Message envoyé">
                        <p><span id="bonjour">Bonjour</span></p>
                        <br />
                        <p class="mb-0">Votre message a bien été envoyé à notre équipe.</p>
                        <p class="mb-0">Nous reviendrons vers vous dans les plus brefs délais.</p>
                        <br />
                        <p>Merci pour votre confiance.</p>
                        <p><b>Plastic Fighters</b></p>
                        <br />
                        <p><a href="/index">Retour à l'accueil</a></p>
                       </div>
                    </div>
                </div>
            </div>
        </div>

    </x-jet-authentication-card>
</x-guest-layout>

    @auth
    <script type="text/javascript"> //Bonjour ou bonsoir selon l'heure de la journée
        today = new Date()
        if (today.getHours() >= 0 && today.getHours() < 18) {
            document.getElementById('bonjour').innerHTML = 'Bonjour,';
        } else {
            document.getElementById('bonjour').innerHTML = 'Bonsoir,';
        }
    </script>
    @endauth
