<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <img src="{{ asset('img/logo.jpg')}}" style="width: 20vh" alt="Plastic Fighters">
    <h2>Message d'un utilisateur</h2>
    <p>Réception d'un mesage ou demande utilisateur :</p>
    <ul>
      <li><strong>Nom</strong> : {{ $contact['name'] }}</li>
      <li><strong>Email</strong> : {{ $contact['email'] }}</li>
      <li><strong>Message</strong> : {{ $contact['message'] }}</li>
    </ul>
  </body>
</html>
