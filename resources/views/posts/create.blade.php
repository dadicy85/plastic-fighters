<x-app-layout>
    <div class="tm-main">
        <div class="tm-section-wrap">
            <div class="tm-parallax" data-parallax="scroll" data-image-src="{{ asset('img/slide4.jpg') }}">
            </div>
            <section id="home" class="tm-section">
                <h2 class="tm-text-primary">Création d'un article</h2>
                <hr class="mb-5">
                <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="mt-4">
                        <x-jet-label for="title" value="{{ __('Titre') }}" />
                        <x-jet-input id="title" class="block mt-1 w-full" type="text" name="title" :value="old('title')"
                            placeholder="Titre" required autofocus />
                    </div>

                    <div class="mt-4">
                        <x-jet-label for="message" value="{{ __('Message') }}" />
                        <textarea id="message" class="block mt-1 w-full" type="text" name="message" cols="30" rows="10"
                            required placeholder="Contenu de l'article"></textarea>
                    </div>

                    <div class="mt-4">
                        <div class="custom-file">
                            <label for="validatedCustomFile" class="custom-file-label">Choisir une image</label>

                            <input type="file" class="custom-file-input @error('message') is-invalid @enderror"
                                name="image" id="validatedCustomFile">
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="flex items-center justify-end mt-4">
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="/index">
                            {{ __('Annuler') }}
                        </a>

                        <x-jet-button class="ml-4">
                            {{ __('Poster') }}
                        </x-jet-button>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</x-app-layout>
