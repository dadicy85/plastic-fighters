@foreach($comments as $comment)
<div class="display-comment" id="comments" @if($comment->parent_id != null) style="margin-left:35px; margin-top:-25px" @endif>

    <div class="media p-3">
        <img src="{{ $comment->user->profile_photo_url }}" alt="{{ $comment->user->name }}" class="mr-3 mt-3 rounded-circle" style="width:40px;">
        <div class="media-body">
          <h6><strong class="username">{{ $comment->user->name }}</strong>&nbsp;&nbsp;<small><i>{{ $comment->created_at->diffForHumans() }}</i></small></h6>

    <div class="options">
        <div class="optionreply">
            <p class="commentaire">{{ $comment->message }}</p>
            @auth
            <a href="" id="reply" class="ml-4 reply" data-bs-toggle="collapse" data-bs-target="#collapse{{ $comment->id }}">
                <i class="far fa-comments"></i>&nbsp;Répondre</a>
            @endauth
        </div>
        <div class="optiondelete">
            {{-- Si l'auth est l'auteur du commentaire, alors il peut le supprimer --}}
            @if (auth()->user() == ($comment->user))
            <form method="POST" action="{{ route('comments.delete', $comment->id )}}"
                onclick=" return confirm('Êtes-vous sur de vouloir supprimer ce commentaire ?')">
                @method('DELETE')
                @csrf
                <button class="ml-4"><i class="far fa-times-circle"></i></button>
            </form>
            @endif
        </div>
    </div>
    </div>
</div>
    <form method="POST" action="{{ route('comments.store') }}">
        @csrf
        <div class="form-group collapse" id="collapse{{ $comment->id }}">
            <x-jet-input type="text" name="message" class="form-control" placeholder="Écrire un commentaire..." />
            <x-jet-input type="hidden" name="post_id" value="{{ $post_id }}" />
            <x-jet-input type="hidden" name="parent_id" value="{{ $comment->id }}" />
        </div>
        {{-- <div class="form-group">
                <x-jet-button class="ml-4">
                    <i class="far fa-comments"></i>&nbsp;
                    {{ __('Répondre') }}
        </x-jet-button>
         </div> --}}
    </form>

@include('posts.commentsDisplay', ['comments' => $comment->replies])

</div>
@endforeach
