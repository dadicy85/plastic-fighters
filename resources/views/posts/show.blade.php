<x-guest-layout>
    <div class="tm-main">
        <div class="tm-section-wrap">
            <div class="tm-parallax" data-parallax="scroll" data-image-src="{{ asset('img/slide1.jpg') }}"></div>
            <section id="home" class="tm-section">

                <div id="section" class="box">
                    <div id="content articles box">
                                <h2 class="tm-text-primary" id="top">{{ $post->title }}</h2>
                                <div class="article-info box">
                                    <p class="f-right"><a href="#comments" class="comment"><i class="far fa-comments"></i>&nbsp;Commentaires ({{ $post->comments()->count() }})</a></p>
                                    <p class="f-left">{{ $post->created_at->format('d/m/Y') }} | Posté par <a
                                            href="#"><b class="username">{{ $post->user->name }}</b></a> | {{ $post->user->role->display_name }}</p>
                                </div>
                                <hr class="mb-5">
                    <div>
                        <img src="{{ URL::to('/uploads/posts/'.$post->image) }}" class="d-block w-100" alt="{{ $post->name }}">
                    </div>
                    <br /><br />
                                <p>{{ $post->message }}</p>
                                <br />
                                <p><a href="{{ URL::to('/uploads/posts/'.$post->image2) }}"><img src="{{ URL::to('/uploads/posts/'.$post->image2) }}" style="width: 50%" alt=""
                                        class="f-left" /></a>{{ $post->message2 }}.</p>
                                <p class="more"><a href="/index" class="btn btn-light">Retour à l'accueil</a></p>
                    </div>
               </div>
                    @auth
                    @if(Auth::user()->id == $post->user_id)
                    <hr />
                    <div class="optionsadmin">
                        <div class="optionadminedit">
                            <!-- {{-- Si l'utilisateur n'est pas l'auteur du post alors il ne peut pas l'éditer --}}-->
                            <p class="more"><a href="/posts/{{ $post->id }}/edit" class="btn btn-light">Éditer l'article</a></p>
                        </div>
                        <div class="optionadmindelete">
                            <form action="{{ route('posts.destroy', $post->id) }}" method="POST"
                                onclick=" return confirm('Êtes-vous sur de vouloir supprimer cet article ?')">
                                @csrf
                                @method('DELETE')
                                <x-jet-button class="ml-4">Supprimer</x-jet-button>
                            </form>
                        </div>
                    </div>
                    @endif
                    @endauth

                <div class="comments">
                    <hr />
                    <h5 class="tm-text-primary"><i class="far fa-comment"></i>&nbsp;Commenter</h5>
                    <hr />
                    @guest
                    <p class="text-center">Si vous voulez poster un commentaire, veuillez vous connecter ou vous
                        inscrire, <a href="/login"><b>ici</b><a>
                                <hr class="tm-hr-short mb-5">
                                @endguest
                                @auth
                                <form method="POST" action="{{ route('comments.store') }}">
                                    @csrf
                                    <div class="form-group">
                                        <x-jet-input type="text" name="message" class="form-control"
                                            placeholder="Écrire un commentaire..." />
                                        <x-jet-input type="hidden" name="post_id" value="{{ $post->id }}" />
                                    </div>

                                    <!--<div class="form-group">-->
                                    <!--<x-jet-button class="ml-4">-->
                                    <!--<i class="far fa-comment"></i>&nbsp;-->
                                    <!--{{ __('Ajouter Commentaire') }}-->
                                    <!--</x-jet-button>-->
                                    <!--</div>-->
                                </form>
                                @endauth

                                @include('posts.commentsDisplay', ['comments' => $post->comments, 'post_id' =>
                                $post->id])
                                <hr />
                                <p class="text-center more"><a href="#top"><b>Retour à l'article</b></a></p>
                </div>
            </div>
        </section>
    </div>
</div>
</x-guest-layout>
