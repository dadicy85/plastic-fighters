<x-guest-layout>

    <div class="tm-main">
        <!-- Accueil -->
        <div class="tm-section-wrap">
            <div class="tm-parallax" data-parallax="scroll" data-image-src="{{ asset('img/slide1.jpg') }}" alt="slide">
            </div>
            <section id="home" class="tm-section clearfix" id="center">
                <h2 class="tm-text-primary">Bienvenue chez les Plastic Fighters</h2>
                <hr class="mb-5">
                <div class="row">
                    <div class="col-lg-6 tm-col-home mb-4">
                        <div class="tm-text-container">
                            <p>
                                Plastic Fighters est un collectif basé en Gironde et créé fin 2020. Sa principale
                                activité est de réaliser des collectes de déchets sur les plages et de sensibiliser les
                                participants à ces collectes, au fléau que sont les micro-plastiques et autres
                                déchets récurrents que l’on trouve sur le sable. Intéressé ? Motivé ? Rejoignez-nous !!
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-6 tm-col-home mb-4">
                        <div class="tm-text-container">
                            <p>
                                Afin de vous donner une idée de ce que à quoi ressemble une journée de collecte de
                                déchets sur les plages,
                                vous avez accès à une galerie de photos ci dessous via ce lien.
                            </p>
                            <br />
                            <div class="text-center">
                                <a href="#gallery" class="btn btn-primary tm-btn-next">Galerie de photos</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="tm-hr-short mb-5">

                <!-- News -->
                <div class="center_8 clearfix">
                    <div class="center_5">
                        <h4 class="tm-text-primary">Quoi de neuf ?</h4>
                    </div>
                    <br />
                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            @foreach($posts as $post)
                            <div class="carousel-item @if($post->id == 1) {{ 'active' }} @endif">
                                <a class="news" href="/posts/{{ $post->id }}">
                                    <img src="{{ asset('/uploads/posts/'.$post->image) }}" class="d-block w-100"
                                        alt="{{ $post->title }}" title="{{ $post->title }}" style="width: 10%"></a>
                                <!--<div class="carousel-caption d-none d-md-block">-->
                                <h4 class="tm-text-primary text-center">{{ $post->title }}</h4>
                                <!--</div>-->
                            </div>
                            @endforeach
                            <button class="carousel-control-prev" type="button"
                                data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button"
                                data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div><br />
                    @auth
                    @if (Auth::user()->hasRole('admin'))
                    <div class="text-center">
                        <a href="/posts/create" class="btn btn-primary" style="margin-bottom: 2vh">Nouvel
                            article</a>
                    </div>
                    @endif
                    @endauth
            </section>
        </div>

        <!-- Medias -->
        <div class="tm-section-wrap">
            <div class="tm-parallax" data-parallax="scroll" data-image-src="img/slide2.jpg"></div>
            <section id="media" class="tm-section">
                <h2 class="tm-text-primary">Dans les Médias</h2>
                <hr class="mb-5">
                <div class="row">
                    @foreach($medias as $media)
                    <div class="media">
                        <img src="{{ asset('/uploads/medias/'.$media->image) }}" class="align-self-center mr-3"
                            alt="{{ $media->title }}" style="width: 15%">
                        <div class="media-body">
                            <h5 class="mt-0">{{ $media->title }}</h5>
                            <p><a href="{{ $media->link }}"
                                    onclick="window.open(this.href);return false">{{ $media->title2 }}</a></p>
                        </div>
                        @auth
                        @if (Auth::user()->hasRole('admin'))
                        <a href="/medias/{{ $media->id }}/edit" class="btn btn-light"
                            style="margin-bottom: 2vh">Éditer</a>
                        @endif
                        @endauth
                    </div>
                    <hr class="tm-hr-short mb-5">
                    @endforeach
                </div>
                <div class="d-flex justify-content-end">
                    {{ $medias->links() }}
                </div>
                <br />
                @auth
                @if (Auth::user()->hasRole('admin'))
                <div class="text-center">
                    <a href="/medias/create" class="btn btn-md btn-primary" style="margin-bottom: 2vh">Nouveau
                        média</a </div> @endif @endauth </section> </div> <!-- Galerie -->
                    <div class="tm-section-wrap">
                        <div class="tm-parallax" data-parallax="scroll" data-image-src="img/slide3.jpg" alt="slide">
                        </div>
                        <section id="gallery" class="tm-section">
                            <h2 class="tm-text-primary">Galerie Photos</h2>
                            <hr class="mb-5">
                            <ul class="tm-gallery-links">
                                <li>
                                    <a href="javascript:void(0);" class="active tm-gallery-link" data-filter="*">
                                        <i class="fas fa-layer-group tm-gallery-link-icon"></i>
                                        Tous
                                    </a>
                                    @foreach($galleries->unique('category') as $gallerie)
                                    <a href="javascript:void(0);" class="tm-gallery-link"
                                        data-filter="{{ $gallerie->category }}">
                                        <i class="fas fa-leaf tm-gallery-link-icon"></i>
                                        {{ $gallerie->category }}
                                    </a>
                                    @endforeach
                                </li>
                            </ul>

                            <div class="tm-gallery">
                                @foreach($galleries as $gallerie)
                                <figure class="effect-honey tm-gallery-item {{ $gallerie->category }}">
                                    <img src="{{ asset('/uploads/gallery/thumb/'.$gallerie->image) }}" />
                                    <figcaption>
                                        <h2><i>{{ $gallerie->category }}</i></h2>
                                        <a href="{{ asset('/uploads/gallery/'.$gallerie->image) }}">Voir plus</a>
                                    </figcaption>
                                </figure>
                                @endforeach
                            </div><br />

                            <!-- Ajouter Photos si Auth -->
                            @auth
                            <div class="text-center">
                                @if (Auth::user()->hasRole('admin'))
                                <x-jet-button data-bs-toggle="collapse" data-bs-target="#galleryform">Ajouter une photo
                                </x-jet-button>
                                <div id="galleryform" class="collapse">
                                    <form method="POST" action="{{ route('image-store') }}" id="image_upload_form"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="mt-4">
                                            <x-jet-label for="caption" value="Image Caption" />
                                            <x-jet-input id="caption" class="block mt-1 w-full" type="text"
                                                name="caption" :value="old('caption')" placeholder="Nom" required
                                                autofocus />
                                        </div>
                                        <div class="mt-4">
                                            <x-jet-label for="caption" value="Liste des albums" />
                                            <select class="form-control form-control-lg" id="category" name="category">
                                                <option value="">Selectionner un album</option>
                                                <option value="Ramassages">Ramassages</option>
                                                <option value="Collectifs">Collectifs</option>
                                                <option value="Affiches">Affiches</option>
                                            </select>
                                        </div>

                                        <div class="mt-4">
                                            <x-jet-label for="caption" value="Upload des photos" />
                                            <div class="dropzone-wrapper">
                                                <div class="dropzone-desc">
                                                    <i class="glyphicon glyphicon-download-alt"></i>
                                                    <p>Choisissez une photo et faites-la glisser ici</p>
                                                </div>
                                                <input type="file" name="image" class="dropzone">
                                            </div>
                                            <div id="image_error"></div>
                                        </div>

                                        <div class="flex items-center justify-end mt-4">
                                            <x-jet-button class="ml-4">
                                                {{ __('Ajouter') }}
                                            </x-jet-button>
                                        </div>
                                    </form>
                                </div>
                                @endif
                                @endauth
                        </section>
                    </div>

                    <!-- A Propos -->
                    <div class="tm-section-wrap">
                        <div class="tm-parallax" data-parallax="scroll" data-image-src="{{ asset('img/slide4.jpg') }}"
                            alt="slide">
                        </div>
                        <section id="about" class="tm-section">
                            <h2 class="tm-text-primary">Qui sommes nous ?</h2>
                            <hr class="mb-5">

                            <div class="row tm-row-about">
                                <div class="tm-col-about tm-col-about-l">
                                    <p class="mb-4">
                                        Composé d’un noyau dur d’une petite quinzaine de personnes, <strong>le collectif
                                            a ramassé
                                            1490kg de
                                            déchets depuis sa création</strong>. Les collectes se font dans la mesure du
                                        possible
                                        une
                                        fois par
                                        mois et à chaque fois, tout se passe dans la bonne humeur. Les mots d’ordre de
                                        Plastic
                                        Fighters: Rigolade, ramassage, quantification, gaudriole, revalorisation et
                                        marrade !!
                                        Ouais on s’marre bien !!!
                                    </p>
                                    <a href="{{ asset('img/bilan2021.png') }}" class="fancylight popup-btn"
                                        data-fancybox-group="light">
                                        <img src="{{ asset('img/bilan2021.png') }}" alt="Bilan Ramassages 2021"
                                            title="Bilan Ramassages 2021">
                                    </a>
                                </div>
                                <div class="tm-col-about tm-col-about-r">
                                    <a href="{{ asset('img/img-about-01.jpg') }}" class="fancylight popup-btn"
                                        data-fancybox-group="light">
                                        <img src="{{ asset('img/img-about-01.jpg') }}" alt="Image"
                                            class="img-fluid mb-5 tm-img-about">
                                    </a>
                                    <p class="mb-4">
                                        Equipé de tamis, sacs de jute et chariot, n’hésitez pas à rejoindre l’aventure,
                                        tout nouveau
                                        participant est bien entendu plus que bienvenu. Si vous n’avez pas de matériel,
                                        on peut
                                        vous en prêter, il ne vous sera demandé de prendre que votre propre paire de
                                        gants.
                                        Prêt à passer à l’action pour la bonne cause ?<br>
                                        Suivez-nous ici ou sur les <a href="#rezo"><strong>réseaux</strong></a> pour
                                        être
                                        courant
                                        de toute nouvelle action !
                                    </p>
                                </div>
                            </div>

                            <hr class="tm-hr-short mb-5">
                            <!-- Staff -->
                            <div class="mt-5">
                                <div class="tm-carousel">
                                    <div class="tm-carousel-item">
                                        <figure class="effect-honey mb-4">
                                            <img src="{{ asset('img/greg.jpg') }}" alt="Featured Item">
                                            <figcaption>
                                                <ul class="tm-social">
                                                    <li><a href="https://www.facebook.com/greg.auriz.988"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-facebook"></i></a></li>
                                                    {{-- <li><a href="https://twitter.com" class="tm-social-link"
                                                onclick="window.open(this.href);return false"><i
                                                    class="fab fa-twitter"></i></a></li>
                                        <li><a href="https://instagram.com" class="tm-social-link"
                                                onclick="window.open(this.href);return false"><i
                                                    class="fab fa-instagram"></i></a></li> --}}
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <div class="tm-about-text">
                                            <h3 class="mb-3 tm-text-primary tm-about-title">Grégory</h3>
                                            <p>Fondateur du collectif et ramasseur assidu. <q><i>J’aime bien les chats
                                                        et l’odeur de
                                                        l’essence.</i></q></p>
                                            <h4 class="tm-text-secondary tm-about-subtitle">Fondateur</h4>
                                        </div>
                                    </div>

                                    <div class="tm-carousel-item">
                                        <figure class="effect-honey mb-4">
                                            <img src="img/fabien.jpg" alt="Featured Item">
                                            <figcaption>
                                                <ul class="tm-social">
                                                    <li><a href="https://www.facebook.com/fabien.bertrand.963"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-facebook"></i></a></li>
                                                    <li><a href="http://linkedin.com/in/fabienbertrand33"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false">
                                                            <i class="fab fa-linkedin-in"></i></a></li>
                                                    {{-- <li><a href="https://instagram.com" class="tm-social-link"
                                                onclick="window.open(this.href);return false"><i
                                                    class="fab fa-instagram"></i></a></li> --}}
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <div class="tm-about-text">
                                            <h3 class="mb-3 tm-text-primary tm-about-title">Fabien</h3>
                                            <p>Co-fondateur du collectif et ci-administrateur de la page FB.
                                                <q><i>J’aime faire des bulles sous l’eau… mais pas à 8h du mat’
                                                        !!!</q></i></p>
                                            <h4 class="tm-text-secondary tm-about-subtitle">Co-fondateur</h4>
                                        </div>
                                    </div>

                                    <div class="tm-carousel-item">
                                        <figure class="effect-honey mb-4">
                                            <img src="img/audrey.jpg" alt="Featured Item">
                                            <figcaption>
                                                <ul class="tm-social">
                                                    <li><a href="https://www.facebook.com/audrey.laurent.568"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-facebook"></i></a></li>
                                                    <li><a href="https://www.instagram.com/drey_laurent/?hl=fr"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-instagram"></i></a></li>
                                                    {{-- <li><a href="https://twitter.com" class="tm-social-link"
                                                onclick="window.open(this.href);return false"><i
                                                    class="fab fa-twitter"></i></a></li> --}}
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <div class="tm-about-text">
                                            <h3 class="mb-3 tm-text-primary tm-about-title">Audrey</h3>
                                            <p>Amoureuse de mère Nature, persuadée que chaque petit geste participe et
                                                incite au
                                                changement.
                                                <q><i>S’est faite des potos en ramassant des ordures !</i></q></p>
                                            <h4 class="tm-text-secondary tm-about-subtitle">Ramasseuse Assidue</h4>
                                        </div>
                                    </div>

                                    <div class="tm-carousel-item">
                                        <figure class="effect-honey mb-4">
                                            <img src="img/damien.jpg" alt="Featured Item">
                                            <figcaption>
                                                <ul class="tm-social">
                                                    <li><a href="https://www.facebook.com/profile.php?id=100010681144522"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-facebook"></i></a></li>
                                                    <li><a href="https://www.linkedin.com/in/damienlaporta/"
                                                            class="tm-social-link"
                                                            onclick="window.open(this.href);return false">
                                                            <i class="fab fa-linkedin-in"></i></a></li>
                                                    {{-- <li><a href="https://instagram.com" class="tm-social-link"
                                                onclick="window.open(this.href);return false"><i
                                                    class="fab fa-instagram"></i></a></li> --}}
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <div class="tm-about-text">
                                            <h3 class="mb-3 tm-text-primary tm-about-title">Damien</h3>
                                            <p>Développeur web ainsi que votre humble serviteur sur ce site fait par mes
                                                petites mains qui ramassent aussi quand l'agenda le permet.
                                                <q><i>Si vous détectez un souci sur ce super site, envoyez BUG au
                                                        81212</i></q>
                                            </p>
                                            <h4 class="tm-text-secondary tm-about-subtitle">Webmaster/
                                                Ramasseur Level 1</h4>
                                        </div>
                                    </div>

                                    <div class="tm-carousel-item">
                                        <figure class="effect-honey mb-4">
                                            <img src="img/alice.jpg" alt="Featured Item">
                                            <figcaption>
                                                <ul class="tm-social">
                                                    <li><a href="https://facebook.com" class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-facebook"></i></a></li>
                                                    <li><a href="https://twitter.com" class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-twitter"></i></a></li>
                                                    <li><a href="https://instagram.com" class="tm-social-link"
                                                            onclick="window.open(this.href);return false"><i
                                                                class="fab fa-instagram"></i></a></li>
                                                </ul>
                                            </figcaption>
                                        </figure>
                                        <div class="tm-about-text">
                                            <h3 class="mb-3 tm-text-primary tm-about-title">Alice</h3>
                                            <p>Passionnée du graphisme ainsi que de la nature. Quoi de mieux que
                                                d’allier les deux pour aider et faire avancer notre cause. <q><i>Le
                                                        plastique c’est pas fantastique à par les licornes, j’adooooore
                                                        les licornes !!</i></q></p>
                                            <h4 class="tm-text-secondary tm-about-subtitle">Graphiste</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <!-- Contact -->
                    <div class="tm-section-wrap">
                        <div class="tm-parallax" data-parallax="scroll" data-image-src="{{ asset('img/slide5.jpg') }}"
                            alt="slide">
                        </div>
                        <div id="contact" class="tm-section">
                            <h2 class="tm-text-primary">Une question ?</h2>
                            <hr class="mb-5">
                            <div class="row">
                                <div class="col-xl-6 tm-contact-col-l mb-4">
                                    <form id="contact-form" action="{{ route('contact') }}" method="POST"
                                        class="tm-contact-form">
                                        @csrf
                                        <div class="form-group">
                                            <div class="form-group">
                                                @if(Auth::user())
                                                <x-jet-input class="block mt-1 w-full" type="text" name="name"
                                                    placeholder="Nom" value="{{ Auth::user()->name }}" required
                                                    autocomplete="name" />
                                                @else
                                                <x-jet-input class="block mt-1 w-full" type="text" name="name"
                                                    placeholder="Nom" value="{{ old('name') }}" required
                                                    autocomplete="name" />
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                @if(Auth::user())
                                                <x-jet-input class="block mt-1 w-full" type="email" name="email"
                                                    placeholder="E-mail" value="{{ Auth::user()->email }}" required
                                                    autocomplete="email" />
                                                @else
                                                <x-jet-input class="block mt-1 w-full" type="email" name="email"
                                                    placeholder="E-mail" value="{{ old('email') }}" required
                                                    autocomplete="email" />
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control" id="contact-select" name="inquiry">
                                                    <option value="-">Sujet</option>
                                                    <option value="participation">Participation</option>
                                                    <option value="renseignement">Renseignement</option>
                                                    <option value="autres">Autres</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <textarea rows="8" name="message" class="form-control rounded-0"
                                                    placeholder="Message" value="{{ old('message') }}"
                                                    required></textarea>
                                            </div>

                                            <div class="flex items-center justify-end mt-4">
                                                <x-jet-button class="ml-4">
                                                    {{ __('Envoyer') }}
                                                </x-jet-button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-xl-6 tm-contact-col-r">
                                <!-- Map -->
                                <div class="mapouter mb-4">
                                    <div class="gmap_canvas">
                                        <iframe width="100%" height="520" id="gmap_canvas"
                                            src="https://www.google.com/maps/embed?origin=mfe&pb=!1m2!2m1!1sGironde,france"
                                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                </div>
                                <!-- Links -->
                                <ul class="tm-contact-links mb-4 text-center">
                                    <li>
                                        <a href="mailto:info@company.com">
                                            <i class="fas fa-at mr-2 tm-contact-link-icon"></i>
                                            Email: plasticfighters33@gmail.com
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
</x-guest-layout>
<script>
    $(document).ready(function () {
        var popup_btn = $('.popup-btn');
        popup_btn.magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });

</script>
