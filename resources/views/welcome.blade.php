<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#87CEFA" />
    <meta name="description" content="Collectif de rammassage des déchets sur les plages.">

    <title>Bienvenue chez les Plastic Fighters</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/welcome.css') }}">

</head>

<body>
    <div class="centered">
        <div>
            <h1 class="jt --debug">
                <span class="jt__row">
                    <a href="/index"><span class="jt__text"><img src="{{ asset('img/plasticfighterslogoblue.jpg') }}"
                                alt="Plastic Fighters" class="logohome"></span></a>
                </span>
                <span class="jt__row jt__row--sibling" aria-hidden="true">
                    <a href="/index"><span class="jt__text"><img src="{{ asset('img/plasticfighterslogoblue.jpg') }}"
                                alt="Plastic Fighters" class="logohome"></span></a>
                </span>
                <span class="jt__row jt__row--sibling" aria-hidden="true">
                    <a href="/index"><span class="jt__text"><img src="{{ asset('img/plasticfighterslogoblue.jpg') }}"
                                alt="Plastic Fighters" class="logohome"></span></a>
                </span>
                <span class="jt__row jt__row--sibling" aria-hidden="true">
                    <a href="/index"><span class="jt__text"><img src="{{ asset('img/plasticfighterslogoblue.jpg') }}"
                                alt="Plastic Fighters" class="logohome"></span></a>
                </span>
            </h1>
        </div>
    </div>
</body>

</html>
