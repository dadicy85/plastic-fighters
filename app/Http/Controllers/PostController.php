<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function __construct() {

        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        // $posts = Post::orderBy('created_at', 'desc')->simplePaginate(5);
        // return view('index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request, [

        // Validation des conditions requises des champs du formulaire
            'title' => 'required|string|min:5|max:50',
            'message' => 'required|string|min:10|max:1000',
            'image' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->message = $request->input('message');
        $post->image = $request->file('image');
        $imageName = time() . $post->image->getClientOriginalName();
        $post->image->move('uploads/posts', $imageName);
        $post->image = $imageName;
        $post->user_id = Auth::user()->id;
        $post->save();

        return redirect('index')->with('success', 'Votre article à été crée !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id){

        $post = Post::with('user')->find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $post = Post::find($id);

    if(auth()->user()->id !== $post->user_id) {

        return redirect('/posts')->with('error', 'Non autorisé');
        }
        return view('posts.edit')->with('post', $post);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $this->validate($request, [

        // Validation des conditions requises des champs du formulaire
            'title' => 'required|string|min:5|max:50',
            'message' => 'required|string|min:10|max:1000',
            'image' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
        ]);

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->message = $request->input('message');
        $post->image = $request->file('image');
        $imageName = time() . $post->image->getClientOriginalName();
        $post->image->move('uploads/posts', $imageName);
        $post->image = $imageName;
        $post->user_id = Auth::user()->id;
        $post->save();

        return back()->with('success', 'Votre article à été modifié !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $post = Post::find($id);

        if(auth()->user()->id !== $post->user_id) {
            return redirect('posts.show')->with('error', 'Non autorisé');
        }

        $post->delete();

        return redirect('/index#home')->with('success', 'Votre article à bien été supprimé !');
    }
}
