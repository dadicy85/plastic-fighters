<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;


class ContactController extends Controller
{
    public function create(){
        return view('contactMail');
    }

    public function store(Request $request){
    // Validation des conditions requises des champs du formulaire
        $request->validate([
            'name'    => 'required|string|min:2|max:50|alpha_dash',
            'email'   => 'required|string|email|min:5|max:100',
            'message' => 'required|string|min:20|max:1000|alpha_dash',
        ]);

    // Envoi du message à l'adresse mail Admin renseignée dans le fichier ENV
        Mail::to(env('MAIL_USERNAME'))
            ->send(new ContactMail($request->except('_token')));

    // Retourne la vue de confirmation d'envoi du message
        return view('emails.confirmeMail');
    }
}
