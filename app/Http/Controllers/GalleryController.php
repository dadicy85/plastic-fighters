<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    public function store(Request $request){

        $request->validate([

        // Validation des conditions requises des champs du formulaire
            'caption' => 'required|string|min:5|max:50',
            'category' => 'required',
            'image' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
        ],[
            'category.required' => 'Merci de choisir une catégorie.'
        ]);

        if($request->hasFile('image')){
          $file=$request->file('image');
          $image_name=rand(1000,9999).time().'.'.$file->getClientOriginalExtension();
          $thumbPath=public_path('uploads/gallery/thumb');
          $resize_image=Image::make($file->getRealPath());
          $resize_image->resize(270,180,function($c){

          })->save($thumbPath.'/'.$image_name);

          $file->move(public_path('uploads/gallery'), $image_name);

        }


Gallery::create([
    'user_id' => Auth::user()->id,
    'caption' => $request->caption,
    'category' => $request->category,
    'image' => $image_name
]);


    return redirect('index#gallery')->with('success', 'Votre photo à bien été uploadée !');
    }
}
