<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Media;
use App\Models\Gallery;

class HomeController extends Controller
{
    public function home(){

        $posts = Post::orderBy('created_at', 'desc')->paginate(3);

        $medias = Media::orderBy('id', 'desc', 100)->paginate(
            $perPage = 4, $columns = ['*'], $pageName = 'medias');

        $galleries = Gallery::orderBy('created_at', 'desc', 100)->paginate(
            $perPage = 6, $columns = ['*'], $pageName = 'galleries');

        return view('index', ['posts' => $posts, 'medias' => $medias, 'galleries' => $galleries]);
    }
}
