<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MediaController extends Controller
{

    public function __construct() {

        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        // $medias = Media::orderBy('created_at', 'desc')->get();
        // return view('index#media', ['medias' => $medias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
        return view('medias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $this->validate($request, [

        // Validation des conditions requises des champs du formulaire
            'title' => 'required|string|min:10|max:500',
            'title2' => 'required|string|min:10|max:500',
            'link' => 'required|string|min:10|max:500',
            'image' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
        ]);

        $media = new Media;
        $media->title = $request->input('title');
        $media->title2 = $request->input('title2');
        $media->link = $request->input('link');
        $media->image = $request->file('image');
        $imageName = time() . $media->image->getClientOriginalName();
        $media->image->move('uploads/medias', $imageName);
        $media->image = $imageName;
        $media->user_id = Auth::user()->id;
        $media->save();

        return redirect('index#media')->with('success', 'Votre média à été crée !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show($id){

        $media = Media::with('user')->find($id);
        return view('medias.show')->with('media', $media);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $media = Media::find($id);

    if(auth()->user()->id !== $media->user_id) {

        return redirect('/medias')->with('error', 'Non autorisé');
        }
        return view('medias.edit')->with('media', $media);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $this->validate($request, [

        // Validation des conditions requises des champs du formulaire
            'title' => 'required|string|min:10|max:500',
            'title2' => 'required|string|min:10|max:500',
            'link' => 'required|string|min:10|max:500',
            'image' => 'required|mimes:bmp,png,jpg,jpeg|max:2048'
        ]);

        $media = Media::find($id);
        $media->title = $request->input('title');
        $media->title2 = $request->input('title2');
        $media->link = $request->input('link');
        $media->image = $request->file('image');
        $imageName = time() . $media->image->getClientOriginalName();
        $media->image->move('uploads/medias', $imageName);
        $media->image = $imageName;
        $media->user_id = Auth::user()->id;
        $media->save();

        return redirect('/index#media')->with('success', 'Votre média à été modifié !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $media = Media::find($id);

        if(auth()->user()->id !== $media->user_id) {
            return redirect('/index#media')->with('error', 'Non autorisé');
        }

        $media->delete();

        return redirect('/index#media')->with('success', 'Votre média à bien été supprimé !');
    }
}

