<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller {

    /**
     * Write Your Code..
     *
     * @return string
    */
    public function store(Request $request) {

        $this->validate($request, [
        // Validation des conditions requises des champs du formulaire
            'message'=>'required',
        ]);

        $comment = $request->all();

        $comment['user_id']=auth()->user()->id;

        Comment::create($comment);

        return back();
    }

    public function destroy($id) {

        $comment = Comment::find($id);

        if (auth()->user() == ($comment->user)) {
            $comment->delete();
        }else{
            return back()->with('error', 'Non autorisé');
        }

        return back();
    }

}
