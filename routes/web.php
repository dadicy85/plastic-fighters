<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use \App\Http\Controllers\PostController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\GalleryController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cgu', function () {
    return view('/cgu');
});


// Route Main
Route::get('index', [HomeController::class, 'home'])->name('index');

// Routes Posts
Route::resource('posts', PostController::class);

// Routes Médias
Route::resource('medias', MediaController::class);

// Route Contact
Route::post('contact', [ContactController::class, 'store'])->name('contact');

// Route Commentaires
Route::post('comments', [CommentController::class, 'store'])->name('comments.store');
Route::delete('comments/{id}', [CommentController::class, 'destroy'])->name('comments.delete');

// Route::get('/test-contact', function () {
//     return new App\Mail\ContactMail([
//       'name' => 'Durand',
//       'email' => 'durand@chezlui.com',
//       'message' => 'Je voulais vous dire que votre site est magnifique !'
//       ]);
// });

// Routes Mails
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');


// Routes Auth
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('profile.show');
})->name('dashboard');

// Route Admin
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/admin')->middleware('admin');

// Routes Galerie Photos
    Route::post('/image-store', [GalleryController::class, 'store'])->name('image-store');
});
